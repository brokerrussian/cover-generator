const $AF = require('../libs/action-flow');
const $mongo = require('../libs/mongo');
const $sha256 = require('sha256');
const $request = require('request');
const $fs = require('fs');
const $path = require('path');
const $Promise = require('bluebird');
const $config = require('config');

const CGPHPService = $config.get('services.cgphp.url');

class Cover {
  static get db () {
    return {
      postCovers: $mongo.collection('postCovers')
    };
  }

  static getHashData (title, image) {
    return $sha256(String(title) + String(image));
  }

  static async get (data) {
    const {
      title, image
    } = data;
    const contentHash = Cover.getHashData(title, image);

    const AF = $AF.create({
      desc: 'get cover',
      hash: contentHash
    });

    async function cancel (err) {
      await AF.end();

      if (err) {
        throw err;
      }
    }

    await AF.await();

    const existsCover = await Cover.db.postCovers.findOne({
      contentHash
    }).catch(cancel);

    if (existsCover) {
      await cancel();
      return existsCover.coverFile;
    }

    const url = await Cover.create(title, image).catch(cancel);
    await cancel();
    return url;
  }

  static async create (title, image) {
    const contentHash = Cover.getHashData(title, image);
    const tmpFileName = contentHash + '.jpg';
    const tmpFileDir = $path.resolve(__dirname, '../../../tmp');
    const tmpFilePath = $path.resolve(tmpFileDir, tmpFileName);

    await new $Promise((resolve, reject) => {
      $fs.open(tmpFilePath, 'w+', (err, fd) => {
        if (err) {
          return reject(err);
        }

        $fs.close(fd, resolve);
      });
    });

    await new $Promise((resolve, reject) => {
      $request
        .post({
          url: CGPHPService,
          form: {
            title,
            image_url: image
          }
        })
        .on('response', () => {
          resolve();
        })
        .on('error', reject)
        .pipe($fs.createWriteStream(tmpFilePath))
    });

    await new $Promise((resolve) => setTimeout(resolve, 500));

    const url = await Cover.uploadFile(tmpFilePath);
    const createdDate = Math.round((new Date()).getTime() / 1000);
    
    await Cover.db.postCovers.insert({
      sourceFile: image,
      coverFile: url,
      contentHash,
      createdDate
    });

    await new $Promise((resolve, reject) => {
      $fs.unlink(tmpFilePath, resolve);
    });

    return url;
  }

  static async uploadFile (tmpFile) {
    const formData = {
      file: $fs.createReadStream(tmpFile)
    };

    const data = await new $Promise((resolve, reject) => {
      $request.post('http://telegra.ph/upload', {formData}, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        const jn = JSON.parse(body);

        if (jn.error) {
          return reject(jn);
        }

        resolve(jn);
      });
    });

    return 'http://telegra.ph' + data[0].src;
  }
}

module.exports = Cover;