<?php

$width = 1200;
$height = 530;

$image_url = isset($_REQUEST["image_url"]) ? $_REQUEST["image_url"] : "http://telegra.ph/file/141fd818f5264be830ddd.png";
$text = isset($_REQUEST["title"]) ? $_REQUEST["title"] : "Test cover generator";

$im = imagecreatetruecolor($width, $height);

$bg = imagecreatefromstring(file_get_contents($image_url));
$bg_w = imagesx($bg);
$bg_h = imagesy($bg);
$scale = 1 / min($bg_w/$width, $bg_h/$height);
$new_w = $bg_w*$scale;
$new_h = $bg_h*$scale;
$x = ($width - $new_w) / 2;
$y = ($height - $new_h) / 2;

imagecopyresized($im, $bg, $x, $y, 0, 0, $new_w, $new_h, $bg_w, $bg_h);
imagefilledrectangle($im, 0, 0, $width, $height, ImageColorAllocatealpha($im, 0, 0, 0, 127/2));

//TEXT
$font_size = 35;
$top = 70;
$left = 40;
$text = wordwrap($text, ($width / $font_size * 2.1) - 10, "\r\n");

Imagettftext($im, $font_size, 0, $left, $top, ImageColorAllocatealpha($im, 255, 255, 255, 0), "./arial.ttf", $text);

//LOGO
$font_size = 50;
$top = $height - $font_size;
$left = $width - $font_size*3 - 35;

Imagettftext($im, $font_size, 0, $left, $top, ImageColorAllocatealpha($im, 255, 255, 255, 0), "./arial.ttf", "DTP");


header("Content-Type: image/jpg");
imagejpeg($im, null, 100);

?>